#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <functional>
#include <assert.h>
#include <Choosers.h>
#include <set>
#include <iterator>
#include "src/FrontierCount.h"

typedef std::string::const_iterator CI;
typedef std::vector<std::pair<CI,CI> > T;
typedef std::vector<T> TVec;

template <typename FUNC>
std::string majorityMerge(const std::vector<std::string>& sset, size_t alphabetSize,
                          FUNC choosingFunction)
{
	using namespace std;

	vector< T > startsWith(alphabetSize);
	for(auto it = sset.cbegin(); it != sset.cend(); ++it)
	{
		startsWith.at( *(it->cbegin()) ).push_back(make_pair(it->cbegin(), it->cend()));
	}

    vector<size_t> frontierCounts(alphabetSize);
    auto getCounts = [](const vector<pair<CI,CI>>& vec) { return vec.size(); };
    transform(startsWith.cbegin(), startsWith.cend(), frontierCounts.begin(), getCounts);

    FrontierCount frontier(frontierCounts);

	string super;
    while(any_of(startsWith.cbegin(), startsWith.cend(), [](const T& v) { return !v.empty(); }))
	{
        auto it = choosingFunction(startsWith.begin(), startsWith.end());

		T temp;
		swap(temp, *it);
		for( auto p : temp)
		{
			auto next = ++p.first;
			if (next != p.second) {
                assert(*next < alphabetSize); // otherwise we will have a bad time
                startsWith[*next].push_back(make_pair(next, p.second));
            }
		}
		super.push_back(distance(startsWith.begin(), it));
        transform(startsWith.cbegin(), startsWith.cend(), frontierCounts.begin(), getCounts);
        frontier.addUpdate(frontierCounts);

	}
    const std::string saveTo("/home/dallas/art/english/asasssssef.dat");
    frontier.printToFile(saveTo);
    const std::string pythonCommand("python /home/dallas/art/english/scs/bar_plotter.py ");
    const std::string systemCallStr = pythonCommand + saveTo;
    system(systemCallStr.c_str());
    return super;
}

bool hasNoSpecialChar(const std::string& word)
{
    return std::all_of(word.cbegin(),word.cend(), [](const char c) { return 64 < c && c < 256; });
}

void asciiOffsetAlphabet(std::string& word)
{

    std::transform(begin(word), end(word), begin(word), [](char c){ return std::tolower(c);});
    std::transform(word.begin(), word.end(), word.begin(), [](char c){ assert(c > 95 && c < 256); return c-96;});
}

void inverseAsciiOffset(std::string& word)
{
    std::transform(word.begin(), word.end(), word.begin(), [](char c){ return c+96; });
}


int main()
{
    using namespace std;
    std::ifstream in_stream ("/usr/share/dict/british-english", std::ifstream::in);
    
   string word;
   vector<string> stringSet;
   while(std::getline(in_stream, word))
	{
		if (hasNoSpecialChar(word))
		{
            asciiOffsetAlphabet(word);
			stringSet.push_back(word);
		}
	}
	auto it = std::max_element(stringSet.cbegin(), stringSet.cend(), [](const std::string& lhs, const std::string& rhs) 
                                                                       { return lhs.size() < rhs.size(); });

    std::string longestWord(*it);
    inverseAsciiOffset(longestWord);
	std::cout << "longest word:" << longestWord << " size:" << longestWord.size() << std::endl;
    auto choosingFunction = [](std::vector<T>::iterator first, std::vector<T>::iterator last) {
        return std::max_element(first, last,
                         [](const T &lhs, const T &rhs) { return lhs.size() < rhs.size(); });
    };

    size_t alphabetSize = 27; // HARD CODED for now
	std::string super = majorityMerge(stringSet, alphabetSize, StartsWithFrequencyChooser());
	std::cout << "superset size:" << super.size() << std::endl;
    std::transform(super.begin(), super.end(), std::ostream_iterator<int>(std::cout, ","), [](const char c)
    {
        return static_cast<int>(c);
    });


//    for(auto i = 0; i != 50; ++i) {
//        std::string superRando = majorityMerge(stringSet, StartsWithFrequencyChooser());
//        std::cout << "superRando size:" << superRando.size() << std::endl;
//        //std::cout << superRando << std::endl;
//    }
	return 0;
}





