
#include <gtest/gtest.h>
#include <IsSubsequence.h>

TEST(IsSubsequence, emptyStringIsSubsequence)
{
    std::string emptyString;
    IsSubsequence isSubsequence(emptyString);
    EXPECT_TRUE(isSubsequence(emptyString));
    std::string anyString("aesntuhFoetuhuase");
    IsSubsequence nonEmptySuper(anyString);
    EXPECT_TRUE(nonEmptySuper(emptyString));
}