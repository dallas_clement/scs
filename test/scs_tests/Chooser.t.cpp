//
// Created by dallas on 14/11/16.
//

#include "gtest/gtest.h"
#include "Choosers.h"

TEST(sillyWalk, aRatherSillyOne)
{
    using namespace std;
    vector<vector<int>> startsWith;
    startsWith.push_back({0, 0});
    startsWith.push_back({});
    StartsWithFrequencyChooser chooser;
    auto it = chooser(startsWith.begin(), startsWith.end());
    EXPECT_EQ(it, startsWith.begin());
}

TEST(sillyWalk, notSoSilly)
{
    using namespace std;
    vector<vector<int>> startsWith{ {0,1}, {}, {1,2,3 } };
    StartsWithFrequencyChooser chooser;
    auto it = chooser(startsWith.begin(), startsWith.end());
    EXPECT_TRUE(it == startsWith.begin() || it == (startsWith.begin()+2));
}
