//
// Created by dallas on 13/11/16.
//

#ifndef ENGLISH_FRONTIERCOUNT_H
#define ENGLISH_FRONTIERCOUNT_H


#include <vector>
#include <cstddef>

class FrontierCount {
public:
    FrontierCount(const std::vector<size_t>& initalCounts);
    void addUpdate(const std::vector<size_t>& updates);
    const std::vector<size_t>& getFrontierAtStep(size_t step);
    void printToFile(const std::string& filename);
    size_t maxStep();
private:
    std::vector<std::vector<size_t>> m_counts;
    size_t m_maxCount;
};


#endif //ENGLISH_FRONTIERCOUNT_H
