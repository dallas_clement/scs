//
// Created by dallas on 14/11/16.
//

#ifndef ENGLISH_SEEDING_H
#define ENGLISH_SEEDING_H


#include <random>
#include <functional>
#include <algorithm>

std::mt19937 seedMT19937();

#endif //ENGLISH_SEEDING_H
