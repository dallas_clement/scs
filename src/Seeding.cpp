//
// Created by dallas on 14/11/16.
//

#include "Seeding.h"

std::mt19937 seedMT19937() {
    std::random_device entropySource;
    const size_t len = std::mt19937::state_size;
    std::vector<std::mt19937::result_type> seeders(len);
    std::generate(std::begin(seeders), std::end(seeders), std::ref(entropySource));
    std::seed_seq seedSeq(std::begin(seeders), std::end(seeders));
    return std::mt19937(seedSeq);
}
