//
// Created by dallas on 24/11/16.
//

#include <algorithm>
#include "IsSubsequence.h"

IsSubsequence::IsSubsequence(const std::string &supersequenceCandidate) : m_super(supersequenceCandidate) {

}

bool IsSubsequence::operator()(const std::string &sequence) {
    auto superIt = m_super.begin();
    for (auto character : sequence)
    {
        superIt = std::find(superIt, m_super.end(), character);
        if (superIt == m_super.end())
            return false;
        superIt++;
    }
    return true;
}
