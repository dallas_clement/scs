//
// Created by dallas on 13/11/16.
//

#include <algorithm>
#include <fstream>
#include <iterator>
#include <assert.h>
#include "FrontierCount.h"

FrontierCount::FrontierCount(const std::vector<size_t> &initalCounts) : m_counts(), m_maxCount(0)
{
    m_counts.push_back(initalCounts);
}

void FrontierCount::addUpdate(const std::vector<size_t>& updates) {
    auto maxIt = std::max_element(updates.begin(), updates.end());
    if (maxIt != updates.end())
        m_maxCount = std::max(m_maxCount, *maxIt);
    m_counts.push_back(updates);
}

const std::vector<size_t> &FrontierCount::getFrontierAtStep(size_t step) {
    return m_counts.at(step);
}

size_t FrontierCount::maxStep() {
    return m_counts.size()-1;
}

void FrontierCount::printToFile(const std::string &filename) {
    std::ofstream ofstream;
    ofstream.open(filename,std::ios::out);
    assert(ofstream.is_open());
    for (const auto& frontier : m_counts)
    {
        std::copy(frontier.begin(), frontier.end(), std::ostream_iterator<int>(ofstream, " "));
        ofstream << m_maxCount << std::endl;
    }
    ofstream.close();

}
