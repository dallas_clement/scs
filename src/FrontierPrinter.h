//
// Created by dallas on 19/11/16.
//

#ifndef ENGLISH_FRONTIERPRINTER_H
#define ENGLISH_FRONTIERPRINTER_H

#include <string>
#include <vector>

class FrontierPrinter {
public:
    FrontierPrinter(const std::string& directoryName);
    void printFrontier(const std::vector<size_t>& frontier, unsigned int index);
private:
    std::string m_directoryName;
};


#endif //ENGLISH_FRONTIERPRINTER_H
