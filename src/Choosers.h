//
// Created by dallas on 14/11/16.
//

#ifndef ENGLISH_CHOOSERS_H
#define ENGLISH_CHOOSERS_H

#include <random>
#include <assert.h>
#include <algorithm>
#include "Seeding.h"


class StartsWithFrequencyChooser
{
public:
    StartsWithFrequencyChooser() : m_gen(seedMT19937())
    {
    }

    template <typename IT>
IT  operator()(IT first, IT last)
{
    using namespace std;
    vector<double> nonZeroWeights;
    for( auto it = first; it != last; it++)
    {
        if (!it->empty())  // Is there a better way to do this??
            nonZeroWeights.push_back(static_cast<double>(it->size()));
    }
    assert(!nonZeroWeights.empty());
    discrete_distribution<> dist(nonZeroWeights.cbegin(), nonZeroWeights.cend());
    auto sample = dist(m_gen);
    auto i = 0;
    auto empty = [](const typename IT::value_type& v) { return v.empty(); };
    auto it = find_if_not(first, last, empty);
    for(; it != last && i != sample; it = find_if_not(++it,last,empty))
    {
        i++;
    }
    assert(it != last);
    return it;
}

private:
    std::mt19937 m_gen;

};

#endif //ENGLISH_CHOOSERS_H
