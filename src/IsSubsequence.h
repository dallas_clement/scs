//
// Created by dallas on 24/11/16.
//

#ifndef ENGLISH_ISSUBSEQUENCE_H
#define ENGLISH_ISSUBSEQUENCE_H

#include <string>

class IsSubsequence {
public:
    IsSubsequence(const std::string& supersequenceCandidate);
    bool operator()(const std::string& sequence);
private:
    const std::string& m_super;
};


#endif //ENGLISH_ISSUBSEQUENCE_H
