//
// Created by dallas on 24/11/16.
//

#ifndef ENGLISH_SUPERSEQUENCEVALIDATOR_H
#define ENGLISH_SUPERSEQUENCEVALIDATOR_H

#include <string>
#include <set>
#include <vector>

class SuperSequenceValidator {
public:
    SuperSequenceValidator(const std::set<std::string>& sequences);
    bool operator()(const std::string& supersequencCandidate);
private:
    std::vector<std::string> m_sequences;
};


#endif //ENGLISH_SUPERSEQUENCEVALIDATOR_H
