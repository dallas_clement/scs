#!/usr/bin/env python
import matplotlib.pyplot as plt
import numpy as np
import sys
import os


def create_plot_filename(filename, index):
    base = filename[:filename.find('.')]
    return base + format(index, '05') + ".jpg"


def make_bar_plot(data, filename):
    ind = np.arange(len(data))
    width = 0.5
    plt.bar(ind, data, width)
    plt.savefig(filename)
    plt.close()


if __name__ == "__main__":
    # get the input args
    if len(sys.argv) < 2:
        sys.exit(1)
    print sys.argv[1]
    filename = sys.argv[1]
    with open(filename) as dataFile:
        contents = dataFile.readlines()
        index = 0
        for content in contents:
            plotFileName = create_plot_filename(filename, index)
            data = [int(n) for n in content.split()]
            make_bar_plot(data, plotFileName)
            index += 1

   # os.remove(filename)
